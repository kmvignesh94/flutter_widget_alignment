import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProviderDemo extends StatefulWidget {
  @override
  _ProviderDemoState createState() => _ProviderDemoState();
}

class _ProviderDemoState extends State<ProviderDemo> {
  ButtonsNotifier _viewModel;

  @override
  void initState() {
    _viewModel = Provider.of(context, listen: false);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print("Build");

    return Scaffold(
        appBar: AppBar(
          title: Text("Provider"),
        ),
        body: Column(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Center(
                child: ChangeNotifierProvider<ButtonData>.value(
                  value: _viewModel.button1,
                  child: Consumer<ButtonData>(
                    builder: (key, data, _) {
                      print("button1 ChangeNotifierProvider");
                      return FlatButton(
                        child: Text(data.text),
                        onPressed: () {
                          _viewModel.updateButton1Text("Updated");
                        },
                      );
                    },
                  ),
                ),
              ),
            ),
            Expanded(
                flex: 1,
                child: Center(
                  child: ChangeNotifierProvider<ButtonData>.value(
                    value: _viewModel.button2,
                    child: Consumer<ButtonData>(
                      builder: (key, data, _) {
                        print("button2 ChangeNotifierProvider");
                        return FlatButton(
                          child: Text(data.text),
                          onPressed: () {
                            _viewModel.updateButton1Text("Updated");
                            _viewModel.updateButton2Text("Updated");
                          },
                        );
                      },
                    ),
                  ),
                )),
            Expanded(
                flex: 1,
                child: Center(
                  child: ChangeNotifierProvider<ButtonData>.value(
                    value: _viewModel.button3,
                    child: Consumer<ButtonData>(
                      builder: (key, data, _) {
                        print("button3 ChangeNotifierProvider");
                        return FlatButton(
                          child: Text(data.text),
                          onPressed: () {
                            _viewModel.updateButton1Text("Updated");
                            _viewModel.updateButton2Text("Updated");
                            _viewModel.updateButton3Text("Updated");
                          },
                        );
                      },
                    ),
                  ),
                )),
          ],
        ));
  }
}

class ButtonsNotifier extends ChangeNotifier {
  ButtonData button1 = ButtonData();
  ButtonData button2 = ButtonData();
  ButtonData button3 = ButtonData();

  updateButton1Text(String newString) {
    button1.text = "Updated";
    button1.notifyListeners();
  }

  updateButton2Text(String newString) {
    button2.text = "Updated";
    button2.notifyListeners();
  }

  updateButton3Text(String newString) {
    button3.text = "Updated";
    button3.notifyListeners();
  }
}

class ButtonData extends ChangeNotifier {
  String text = "Button";
}
