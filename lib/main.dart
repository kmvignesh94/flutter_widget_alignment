import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutterdemo/ProviderExample.dart';
import 'package:provider/provider.dart';

Color colorPrimary = Color(0xFF6658AF);
Color colorSecondary = Color(0xFF11EFE6);
Color colorAccent = Color(0xFFFA236E);

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
//      home: ChangeNotifierProvider<ButtonsNotifier>(
//          create: (_) => ButtonsNotifier(), child: ProviderDemo()),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  bool showRow = true;
  bool _mainAxisSize = false;
  TextStyle whiteText = TextStyle(color: Colors.white);
  TextStyle white70Text = TextStyle(color: Colors.white70);

  int _mainAxisIndex = 0;
  int _crossAxisIndex = 0;

  int getNextIndex(int currentIndex, int length) {
    currentIndex = currentIndex + 1;
    return currentIndex % length;
  }

  int getPreviousIndex(int currentIndex, int length) {
    if (currentIndex > 0) {
      currentIndex = currentIndex - 1;
      return currentIndex % length;
    } else {
      return length - 1;
    }
  }

  String getMainAxisAlignment(MainAxisAlignment alignment) {
    return alignment.toString().replaceAll("MainAxisAlignment.", "");
  }

  String getCrossAxisAlignment(CrossAxisAlignment alignment) {
    return alignment.toString().replaceAll("CrossAxisAlignment.", "");
  }

  @override
  Widget build(BuildContext context) {
    return _demoView();
  }

  Widget getRow(String courseName, String completionPercentage,
      String completionText, String scorePercentage, String scoreText) {
    return IntrinsicHeight(
      child:
          Row(crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
        Expanded(
          flex: 1,
          child: Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              courseName,
              style: TextStyle(color: Color(0xFF666666), fontSize: 12),
            ),
          ),
        ),
        Opacity(
          opacity: 0.3,
          child: VerticalDivider(
            color: Colors.black,
            indent: 10,
            endIndent: 10,
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  completionPercentage,
                  style: TextStyle(
                      color: colorPrimary,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                ),
                Text(
                  completionText,
                  style: TextStyle(fontSize: 8, color: Color(0x88414141)),
                )
              ],
            ),
          ),
        ),
        Opacity(
          opacity: 0.3,
          child: VerticalDivider(
            color: Colors.black,
            indent: 10,
            endIndent: 10,
          ),
        ),
        Expanded(
          flex: 1,
          child: Container(
            padding: const EdgeInsets.all(8.0),
            child: scorePercentage == "0%"
                ? Text(
                    "There are no scores in this course",
                    style: TextStyle(fontSize: 8, color: Color(0x88414141)),
                  )
                : Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        scorePercentage,
                        style: TextStyle(
                            color: colorAccent,
                            fontSize: 20,
                            fontWeight: FontWeight.bold),
                      ),
                      Text(
                        scoreText,
                        style: TextStyle(fontSize: 8, color: Color(0x88414141)),
                      )
                    ],
                  ),
          ),
        ),
      ]),
    );
  }

  Widget _demoView() {
    return Scaffold(
      appBar: AppBar(
        title: Text("Row & Column"),
        bottom: PreferredSize(
            preferredSize: Size(double.infinity, 150),
            child: Container(
              padding: EdgeInsets.all(8),
              color: Colors.blue,
              child: Column(
                children: <Widget>[
                  Opacity(
                    opacity: .3,
                    child: Divider(
                      indent: 16,
                      endIndent: 16,
                      height: 1,
                      color: Colors.white70,
                      thickness: 2,
                    ),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Column(
                          children: <Widget>[
                            Text(
                              "Widget",
                              style: white70Text,
                            ),
                            Row(
                              children: <Widget>[
                                SizedBox(
                                  width: 50,
                                  child: FlatButton(
                                    child: Icon(
                                      Icons.chevron_left,
                                      color: Colors.white,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        showRow = !showRow;
                                      });
                                    },
                                  ),
                                ),
                                Expanded(
                                  child: Center(
                                    child: Text((showRow ? "Row" : "Column"),
                                        style: whiteText),
                                  ),
                                ),
                                SizedBox(
                                  width: 50,
                                  child: FlatButton(
                                    child: Icon(
                                      Icons.chevron_right,
                                      color: Colors.white,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        showRow = !showRow;
                                      });
                                    },
                                  ),
                                ),
                              ],
                            ),
                            Text("MainAxisAlignment", style: white70Text),
                            Row(
                              children: <Widget>[
                                SizedBox(
                                  width: 50,
                                  child: FlatButton(
                                    child: Icon(
                                      Icons.chevron_left,
                                      color: Colors.white,
                                    ),
                                    onPressed: () {
                                      int newIndex = getPreviousIndex(
                                          _mainAxisIndex,
                                          MainAxisAlignment.values.length);
                                      setState(() {
                                        _mainAxisIndex = newIndex;
                                      });
                                    },
                                  ),
                                ),
                                Expanded(
                                  child: Center(
                                    child: Text(
                                        getMainAxisAlignment(MainAxisAlignment
                                            .values[_mainAxisIndex]),
                                        style: whiteText),
                                  ),
                                ),
                                SizedBox(
                                  width: 50,
                                  child: FlatButton(
                                    child: Icon(
                                      Icons.chevron_right,
                                      color: Colors.white,
                                    ),
                                    onPressed: () {
                                      int newIndex = getNextIndex(
                                          _mainAxisIndex,
                                          MainAxisAlignment.values.length);
                                      setState(() {
                                        _mainAxisIndex = newIndex;
                                      });
                                    },
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Column(
                          children: <Widget>[
                            Text("MainAxisSize", style: white70Text),
                            Row(
                              children: <Widget>[
                                SizedBox(
                                  width: 50,
                                  child: FlatButton(
                                    child: Icon(
                                      Icons.chevron_left,
                                      color: Colors.white,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _mainAxisSize = !_mainAxisSize;
                                      });
                                    },
                                  ),
                                ),
                                Expanded(
                                  child: Center(
                                    child: Text(_mainAxisSize ? "Max" : "Min",
                                        style: whiteText),
                                  ),
                                ),
                                SizedBox(
                                  width: 50,
                                  child: FlatButton(
                                    child: Icon(
                                      Icons.chevron_right,
                                      color: Colors.white,
                                    ),
                                    onPressed: () {
                                      setState(() {
                                        _mainAxisSize = !_mainAxisSize;
                                      });
                                    },
                                  ),
                                ),
                              ],
                            ),
                            Text("CrossAxisAlignment", style: white70Text),
                            Row(
                              children: <Widget>[
                                SizedBox(
                                  width: 50,
                                  child: FlatButton(
                                    child: Icon(
                                      Icons.chevron_left,
                                      color: Colors.white,
                                    ),
                                    onPressed: () {
                                      int newIndex = getPreviousIndex(
                                          _crossAxisIndex,
                                          CrossAxisAlignment.values.length);
                                      setState(() {
                                        _crossAxisIndex = newIndex;
                                      });
                                    },
                                  ),
                                ),
                                Expanded(
                                  child: Center(
                                    child: Text(
                                        getCrossAxisAlignment(CrossAxisAlignment
                                            .values[_crossAxisIndex]),
                                        style: whiteText),
                                  ),
                                ),
                                SizedBox(
                                  width: 50,
                                  child: FlatButton(
                                    child: Icon(
                                      Icons.chevron_right,
                                      color: Colors.white,
                                    ),
                                    onPressed: () {
                                      int newIndex = getNextIndex(
                                          _crossAxisIndex,
                                          CrossAxisAlignment.values.length);
                                      setState(() {
                                        _crossAxisIndex = newIndex;
                                      });
                                    },
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            )),
      ),
      body: Container(
          child: showRow
              ? Container(
                  color: Colors.yellow,
                  child: Row(
                    textBaseline: TextBaseline.ideographic,
                    mainAxisAlignment: MainAxisAlignment.values[_mainAxisIndex],
                    crossAxisAlignment:
                        CrossAxisAlignment.values[_crossAxisIndex],
                    mainAxisSize:
                        _mainAxisSize ? MainAxisSize.max : MainAxisSize.min,
                    children: _getListWidget(),
                  ),
                )
              : Container(
                  color: Colors.yellow,
                  child: Column(
                    textBaseline: TextBaseline.ideographic,
                    mainAxisAlignment: MainAxisAlignment.values[_mainAxisIndex],
                    crossAxisAlignment:
                        CrossAxisAlignment.values[_crossAxisIndex],
                    mainAxisSize:
                        _mainAxisSize ? MainAxisSize.max : MainAxisSize.min,
                    children: _getListWidget(),
                  ),
                )),
    );
  }

  List<Widget> _getListWidget() {
    return [
      Container(
          width: 50,
          height: 50,
          color: Colors.green,
          child: Icon(
            Icons.skip_previous,
            size: 50,
          )),
      Container(
          width: 100,
          height: 100,
          color: Colors.blue,
          child: Icon(
            Icons.security,
            size: 100,
          )),
      Container(
          width: 50,
          height: 50,
          color: Colors.green,
          child: Icon(Icons.skip_next, size: 50)),
      Text(
        "1234",
        style: TextStyle(fontSize: 30),
      )
    ];
  }
}
